#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <gmp.h>
#include <pthread.h>

typedef struct tsafenode {
    struct tsafenode *next;
    mpz_t number;
    } TSAFENODE;

typedef struct tsafelist {
    pthread_mutex_t *mutex;
    TSAFENODE *head;
    
} TSAFELIST;

typedef struct tsafereturndata {
// True(1)/False(0) if returned data is valid.
    int isValid;
    mpz_t value;
    } TSAFEDATA;
TSAFELIST* tSafeConstruct();

void tSafeDestruct(TSAFELIST* list);

void tSafeEnqueue(TSAFELIST *list,mpz_t number);

TSAFEDATA tSafeDequeue(TSAFELIST* list);