#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <gmp.h>
#include <pthread.h>
#include <unistd.h>
#include "prog2_1.h"

TSAFELIST* tSafeConstruct(){

    TSAFELIST* list = (TSAFELIST*) malloc(sizeof(TSAFELIST));
    list->mutex = (pthread_mutex_t*) malloc(sizeof(pthread_mutex_t));

    pthread_mutex_init(list->mutex,NULL); 
    list->head = NULL; //set head to null
    return list;
}

void tSafeDestruct(TSAFELIST* list){
    pthread_mutex_lock(list->mutex);// lock thread

    while(list->head != NULL){
        TSAFENODE *tmpNode = list->head;
        list->head = list->head->next;// dequeue the entire list until head is null
        free(tmpNode);
    }

    pthread_mutex_unlock(list->mutex);// unlock thread
    free(list->mutex);
    free(list);

}

void tSafeEnqueue(TSAFELIST * list,mpz_t number){ // chaned mpz_t to mpz_t number to make things easier   
   
    TSAFENODE *tmpNode = (TSAFENODE*)malloc(sizeof(TSAFENODE));
    mpz_init(tmpNode->number);
    mpz_set(tmpNode->number, number);
    tmpNode->next = NULL;
    pthread_mutex_lock(list->mutex);// lock thread

    if(list->head == NULL){
        list->head = tmpNode;
        }

    else{
        TSAFENODE *tmpNode1 = list->head;

        while(tmpNode1->next != NULL){
            tmpNode1 = tmpNode1->next;       
            }
        tmpNode1->next = tmpNode;
    }
    pthread_mutex_unlock(list->mutex); //unlock thread 
}

TSAFEDATA tSafeDequeue(TSAFELIST* list){
    TSAFEDATA tmp;
    mpz_init(tmp.value);

    pthread_mutex_lock(list->mutex); //lock thread    
    if(list->head == NULL){
       tmp.isValid = 0; // if head is null isValid is 0 which means its false.
    }
    else{
        mpz_set(tmp.value,list->head->number);
        tmp.isValid = 1;
        TSAFENODE *tmpNode = list->head; // tmpNode is list head
        list->head = list->head->next;        
        mpz_clear(tmpNode->number);
        free(tmpNode);
    }
    pthread_mutex_unlock(list->mutex); //lock thread
    return tmp;
}