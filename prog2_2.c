#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <gmp.h>
#include <pthread.h>
#include <unistd.h>
#include "prog2_1.h"

mpz_t globalCounter;
TSAFELIST *numberList;
pthread_mutex_t counterGuard =  PTHREAD_MUTEX_INITIALIZER;

 void getNextNum(mpz_t* num){

    pthread_mutex_lock(&counterGuard);
    mpz_add_ui(globalCounter,globalCounter,1);// increments the global counter by 1.
    mpz_set(*num,globalCounter);// makes a copy of global counter into nextNum.
    pthread_mutex_unlock(&counterGuard);
}

void * generatePrimes(void *args){
    mpz_t *tmp = malloc(sizeof(mpz_t));
    mpz_init(*tmp);   
    
    while(1){
        getNextNum(tmp);// set temp to getNextNum

        if(mpz_probab_prime_p(*tmp,100000) > 0){
            tSafeEnqueue(numberList,*tmp); // if psuedoPrime enqueque
        }
    usleep(1);
    }
    mpz_clear(*tmp);
    free(tmp);
}

int main(int numArgs, char* argv[]){
  printf("Assignment #2-2, Yajush Sharma, yajushsharma1@gmail.com\n");// print this
    int k = atoi(argv[1]);// takes in the first argument
    int B = atoi(argv[2]);// takes in the second argument
    mpz_ui_pow_ui(globalCounter,2,B-1);

    numberList = tSafeConstruct();
    pthread_t t1;
    pthread_create(&t1,NULL,generatePrimes,0);// creates t1 then calls generatePrimes(void *arg)
    int dequeNumber = 0;

    while(1)
    {
       TSAFEDATA dequeNum = tSafeDequeue(numberList);
       if(dequeNum.isValid == 1){
           mpz_out_str(stdout, 10, dequeNum.value);
           printf("\n");
           dequeNumber++;
          }
       if(dequeNumber >= k){
           break;
         }
     usleep(1);
    }
}